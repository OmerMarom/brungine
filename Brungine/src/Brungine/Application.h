#pragma once

#include "Core.h"
#include "Window.h"

namespace Brungine
{
	class BRUNGINE_API Application
	{
	public:
		Application();

		virtual ~Application();

		void run();

	private:
		std::unique_ptr<Window> mWindow;
		bool mRunning = true;
	};

	// This function is defined by the lib user.
	Application* createApplication();
}
