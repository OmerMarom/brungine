#pragma once

#include "Core.h"
#include <spdlog/spdlog.h>
#include "spdlog/fmt/ostr.h"

namespace Brungine
{
	class BRUNGINE_API Log
	{
	public:
		static void init();

		inline static std::shared_ptr<spdlog::logger>& getCoreLogger() { return mCoreLogger; }

		inline static std::shared_ptr<spdlog::logger>& getClientLogger() { return mClientLogger; }
		
	private:
		static std::shared_ptr<spdlog::logger> mCoreLogger;
		static std::shared_ptr<spdlog::logger> mClientLogger;
	};
}

// Core log macros:
#define BRU_CORE_TRACE(...)   ::Brungine::Log::getCoreLogger()->trace(__VA_ARGS__)
#define BRU_CORE_INFO(...)    ::Brungine::Log::getCoreLogger()->info(__VA_ARGS__)
#define BRU_CORE_WARN(...)    ::Brungine::Log::getCoreLogger()->warn(__VA_ARGS__)
#define BRU_CORE_ERROR(...)   ::Brungine::Log::getCoreLogger()->error(__VA_ARGS__)
#define BRU_CORE_FATAL(...)   ::Brungine::Log::getCoreLogger()->critical(__VA_ARGS__)

// Client log macros:
#define BRU_TRACE(...)        ::Brungine::Log::getClientLogger()->trace(__VA_ARGS__)
#define BRU_INFO(...)         ::Brungine::Log::getClientLogger()->info(__VA_ARGS__)
#define BRU_WARN(...)         ::Brungine::Log::getClientLogger()->warn(__VA_ARGS__)
#define BRU_ERROR(...)        ::Brungine::Log::getClientLogger()->error(__VA_ARGS__)
#define BRU_FATAL(...)        ::Brungine::Log::getClientLogger()->critical(__VA_ARGS__)