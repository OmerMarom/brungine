#pragma once

#include "brupch.h"

#include "Brungine/Core.h"
#include "Brungine/Events/Event.h"

#define DEFAULT_WIDTH  1280
#define DEFAULT_HEIGHT 720

namespace Brungine {

	struct WindowProps
	{
		WindowProps(const std::string& title = "Brungine", 
					unsigned int width = DEFAULT_WIDTH,
					unsigned int height = DEFAULT_HEIGHT) :
			title(title),
			width(width),
			height(height)
		{ }

		std::string title;
		unsigned int width;
		unsigned int height;
	};

	// Interface representing a desktop system based Window
	class BRUNGINE_API Window
	{
	public:
		using EventCallbackFn = std::function<void(Event&)>;
		
		Window() = default;

		virtual ~Window() = default;

		virtual void onUpdate() = 0;

		virtual unsigned int getWidth() const = 0;

		virtual unsigned int getHeight() const = 0;

		virtual void setEventCallback(const EventCallbackFn& callback) = 0;

		virtual void setVSync(bool enabled) = 0;

		virtual bool isVSync() const = 0;

		static Window* create(const WindowProps& props = WindowProps());
	};

}