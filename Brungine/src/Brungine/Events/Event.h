#pragma once

#include "Brungine/Core.h"

namespace Brungine
{
	enum class EventType
	{
		None = 0,
		// Window events:
		WindowClose, WindowResize, WindowFocus, WindowLostFocus, WindowMoved,
		// App events:
		AppTick, AppUpdate, AppRender,
		// Keyboard events:
		KeyPressed, KeyReleased,
		// Mouse events:
		MouseButtonPressed, MouseButtonReleased, MouseMoved, MouseScrolled
	};

	enum EventCategory
	{
		None = 0,
		EventCategoryApplication = BIT(0),
		EventCategoryInput =       BIT(1),
		EventCategoryKeyboard =    BIT(2),
		EventCategoryMouse =       BIT(3),
		EventCategoryMouseButton = BIT(4)
	};

	class BRUNGINE_API Event
	{	
	public:
		Event() = default;

		virtual ~Event() = default;

		virtual EventType getEventType() const = 0;

		virtual const char* getName() const = 0;

		virtual int getCategoryFlags() const = 0;

		virtual std::string toString() const { return getName(); }

		inline bool isInCategory(EventCategory category)
		{
			return getCategoryFlags() & category;
		}

	protected:
		bool mHandled = false;
	};

	// Make event printable:
	inline std::ostream& operator<<(std::ostream& os, const Event& event)
	{
		return os << event.toString();
	}
}

// Repetetive impl. code in Event's subclasses:
#define EVENT_CLASS_TYPE(type) static EventType getStaticType() { return EventType::##type; }\
							   virtual EventType getEventType() const override { return getStaticType(); }\
							   virtual const char* getName() const override { return #type; }

#define EVENT_CLASS_CATEGORY(category) virtual int getCategoryFlags() const { return category; }
