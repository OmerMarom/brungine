#pragma once

#include "Event.h"

namespace Brungine
{

	class BRUNGINE_API WindowResizeEvent : public Event
	{
	public:
		WindowResizeEvent(unsigned int width, unsigned int height) :
			mWidth(width),
			mHeight(height) 
		{ }

		virtual ~WindowResizeEvent() = default;

		inline unsigned int getWidth() const { return mWidth; }

		inline unsigned int getHeight() const { return mHeight; }

		std::string toString() const override
		{
			std::stringstream ss;
			ss << "WindowResizeEvent: " << mWidth << ", " << mHeight;
			return ss.str();
		}

		EVENT_CLASS_TYPE(WindowResize)
		EVENT_CLASS_CATEGORY(EventCategoryApplication)

	private:
		unsigned int mWidth, mHeight;
	};

	class BRUNGINE_API WindowCloseEvent : public Event
	{
	public:
		WindowCloseEvent() { }

		virtual ~WindowCloseEvent() = default;

		EVENT_CLASS_TYPE(WindowClose)
		EVENT_CLASS_CATEGORY(EventCategoryApplication)
	};

	class BRUNGINE_API AppTickEvent : public Event
	{
	public:
		AppTickEvent() { }

		virtual ~AppTickEvent() = default;

		EVENT_CLASS_TYPE(AppTick)
		EVENT_CLASS_CATEGORY(EventCategoryApplication)
	};

	class BRUNGINE_API AppUpdateEvent : public Event
	{
	public:
		AppUpdateEvent() {}

		virtual ~AppUpdateEvent() = default;

		EVENT_CLASS_TYPE(AppUpdate)
		EVENT_CLASS_CATEGORY(EventCategoryApplication)
	};

	class BRUNGINE_API AppRenderEvent : public Event
	{
	public:
		AppRenderEvent() {}

		virtual ~AppRenderEvent() = default;

		EVENT_CLASS_TYPE(AppRender)
		EVENT_CLASS_CATEGORY(EventCategoryApplication)
	};
}
