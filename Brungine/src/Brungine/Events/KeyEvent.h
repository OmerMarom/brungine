#pragma once

#include "Event.h"

namespace Brungine {

	class BRUNGINE_API KeyEvent : public Event
	{
	public:
		virtual ~KeyEvent() = default;

		inline int getKeyCode() const { return mKeyCode; }

		EVENT_CLASS_CATEGORY(EventCategoryKeyboard | EventCategoryInput)

	protected:
		KeyEvent(int keycode) : mKeyCode(keycode) { }

		int mKeyCode;
	};

	class BRUNGINE_API KeyPressedEvent : public KeyEvent
	{
	public:
		KeyPressedEvent(int keycode, int repeatCount) : 
			KeyEvent(keycode),
			mRepeatCount(repeatCount)
		{ }

		virtual ~KeyPressedEvent() = default;

		inline int getRepeatCount() const { return mRepeatCount; }

		std::string toString() const override
		{
			std::stringstream ss;
			ss << "KeyPressedEvent: " << mKeyCode << " (" << mRepeatCount << " repeats)";
			return ss.str();
		}

		EVENT_CLASS_TYPE(KeyPressed)

	private:
		int mRepeatCount;
	};

	class BRUNGINE_API KeyReleasedEvent : public KeyEvent
	{
	public:
		KeyReleasedEvent(int keycode) : KeyEvent(keycode) { }

		virtual ~KeyReleasedEvent() = default;

		std::string toString() const override
		{
			std::stringstream ss;
			ss << "KeyReleasedEvent: " << mKeyCode;
			return ss.str();
		}

		EVENT_CLASS_TYPE(KeyReleased)
	};
}