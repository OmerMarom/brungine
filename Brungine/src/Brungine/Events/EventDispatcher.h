#pragma once

#include "Event.h"

namespace Brungine
{
	class EventDispatcher
	{
		template<typename T>
		using EventFn = std::function<bool(T&)>;
	public:
		EventDispatcher(Event& event) : mEvent(event) { }

		virtual ~EventDispatcher() = default;

		template<typename T>
		bool dispatch(EventFn<T> func)
		{
			if (mEvent.getEventType() == T::getStaticType())
			{
				mEvent.mHandled = func(*(T*)&mEvent);
				return true;
			}
			return false;
		}
	private:
		Event& mEvent;
	};
}
