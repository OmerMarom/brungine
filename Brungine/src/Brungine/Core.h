#pragma once

#ifdef BRU_PLATFORM_WINDOWS
	#ifdef BRU_BUILD_DLL
		#define BRUNGINE_API __declspec(dllexport)
	#else
		#define BRUNGINE_API __declspec(dllimport)
	#endif
#else
	#error Brungine only supports Windows!
#endif

#ifdef BRU_ENABLE_ASSERTS
	#define BRU_ASSERT(x, ...) { if(!(x)) { BRU_ERROR("Assertion Failed: {0}", __VA_ARGS__); __debugbreak(); } }
	#define BRU_CORE_ASSERT(x, ...) { if(!(x)) { BRU_CORE_ERROR("Assertion Failed: {0}", __VA_ARGS__); __debugbreak(); } }
#else
	#define BRU_ASSERT(x, ...)
	#define BRU_CORE_ASSERT(x, ...)
#endif

#define BIT(x) (1 << x) 
