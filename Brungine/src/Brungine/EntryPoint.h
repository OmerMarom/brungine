#pragma once

#include "brupch.h"

#ifdef BRU_PLATFORM_WINDOWS

extern Brungine::Application* Brungine::createApplication();

int main(int argc, char** argv)
{
	Brungine::Log::init();

	auto app = Brungine::createApplication();
	app->run();
	delete app;
}

#endif
