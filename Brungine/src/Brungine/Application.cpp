#include "brupch.h"

#include "Application.h"

#include "Brungine/Events/ApplicationEvent.h"
#include <GLFW/glfw3.h>

namespace Brungine
{
	Application::Application() : 
		mWindow(Window::create())
	{ }

	Application::~Application() { }
	
	void Application::run()
	{
		while (mRunning)
		{
			glClearColor(1, 0, 1, 1);
			glClear(GL_COLOR_BUFFER_BIT);
			mWindow->onUpdate();
		}
	}
}
