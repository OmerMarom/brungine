#pragma once

#include "Brungine/Window.h"
#include <GLFW/glfw3.h>

namespace Brungine {

	class WindowsWindow : public Window
	{
	public:
		WindowsWindow(const WindowProps& props);

		virtual ~WindowsWindow();

		void onUpdate() override;

		inline unsigned int getWidth() const override { return mData.width; }
		inline unsigned int getHeight() const override { return mData.height; }

		// Window attributes
		inline void setEventCallback(const EventCallbackFn& callback) override 
		{ 
			mData.eventCallback = callback;
		}
		
		void setVSync(bool enabled) override;

		inline bool isVSync() const override { return mData.vSync; };

	private:
		virtual void init(const WindowProps& props);

		virtual void shutdown();

		struct WindowData
		{
			std::string title;
			unsigned int width, height;
			bool vSync;
			EventCallbackFn eventCallback;
		};

		WindowData mData;
		GLFWwindow* mWindow;
	};
}