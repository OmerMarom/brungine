#pragma once

// For use by lib users.

// Base Application class to be subclassed by user:
#include "Brungine/Application.h"

// Engine entry point:
#include "Brungine/EntryPoint.h"

// Logging utils:
#include "Brungine/Log.h"
