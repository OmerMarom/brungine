workspace "Brungine"
	architecture "x64"

	configurations
	{
		"Debug",
		"Release",
		"Dist"
	}

-- For example, "Debug-windows-x64"
outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

IncludeDir = {}
IncludeDir["GLFW"] = "Brungine/dependencies/GLFW/include"

include "Brungine/dependencies/GLFW"

project "Brungine"
	location "Brungine"
	kind "SharedLib"
	language "C++"

	targetdir ("bin/" .. outputdir .. "/%{prj.name}")
	objdir ("bin-int/" .. outputdir .. "/%{prj.name}")

	pchheader "brupch.h"
	pchsource "%{prj.name}/src/brupch.cpp"

	files
	{
		"%{prj.name}/src/**.h",
		"%{prj.name}/src/**.cpp"
	}

	includedirs
	{
		"%{prj.name}/src",
		"%{prj.name}/dependencies/spdlog/include",
		"%{IncludeDir.GLFW}"
	}

	links 
	{ 
		"GLFW",
		"opengl32.lib"
	}

	filter "system:windows"
		cppdialect "C++17"
		staticruntime "On"
		systemversion "latest"
		
		defines
		{
			"BRU_PLATFORM_WINDOWS",
			"BRU_BUILD_DLL"
		}

		postbuildcommands
		{
			-- Copy Brungine .dll to Sandbox .exe directory.
			("{COPY} %{cfg.buildtarget.relpath} ../bin/" .. outputdir .. "/Sandbox")
		}

	filter "configurations:Debug"
		defines "BRU_DEBUG"
		symbols "On"

	filter "configurations:Release"
		defines "BRU_RELEASE"
		optimize "On"
		symbols "On"

	filter "configurations:Dist"
		defines "BRU_DIST"
		optimize "On"

project "Sandbox"
	location "Sandbox"
	kind "ConsoleApp"
	language "C++"

	targetdir ("bin/" .. outputdir .. "/%{prj.name}")
	objdir ("bin-int/" .. outputdir .. "/%{prj.name}")

	files
	{
		"%{prj.name}/src/**.h",
		"%{prj.name}/src/**.cpp"
	}

	includedirs
	{
		"Brungine/dependencies/spdlog/include",
		"Brungine/src"
	}

	links
	{
		"Brungine"
	}

	filter "system:windows"
		cppdialect "C++17"
		staticruntime "On"
		systemversion "latest"
		
		defines
		{
			"BRU_PLATFORM_WINDOWS",
		}

	filter "configurations:Debug"
		defines "BRU_DEBUG"
		symbols "On"

	filter "configurations:Release"
		defines "BRU_RELEASE"
		optimize "On"
		symbols "On"

	filter "configurations:Dist"
		defines "BRU_DIST"
		optimize "On"