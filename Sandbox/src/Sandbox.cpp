#include <Brungine.h>

class Sandbox : public Brungine::Application
{
public:
	Sandbox() { }
	~Sandbox() { }
};

Brungine::Application* Brungine::createApplication()
{
	return new Sandbox();
}